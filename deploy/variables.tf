variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "engleosantos@hotmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Passowrd for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "103325067287.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops2:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for PROXY"
  default     = "103325067287.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy2:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django App"
}